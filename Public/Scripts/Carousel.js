// -----JS CODE-----
//@input Component.Image[] carouselElements;
//@input Asset.Texture[] mapPiecesTextures;
//@input SceneObject carouselRArrow;
//@input SceneObject carouselLArrow;

global.ResetCarousel=function(){
    global.tweenManager.resetObject(script.carouselRArrow,"RArrowTween")
    global.tweenManager.resetObject(script.carouselRArrow,"CarouselDissolve")
     global.tweenManager.resetObject(script.carouselLArrow,"LArrowTween")
  global.SetCarouselIndex(1)
}

function SetCarouselElementTexture(elementID,textureID)
{
    script.carouselElements[elementID].mainPass.baseTex=script.mapPiecesTextures[textureID];
}

global.SetCarouselElementShadowMode=function SetCarouselElementShadowMode(alphaValue,index) {
  var currCarouselColor =
    script.carouselElements[index].mainPass.baseColor;
  
  script.carouselElements[index].mainPass.baseColor = new vec4(
    currCarouselColor.r,
    currCarouselColor.g,
    currCarouselColor.b,
    alphaValue
  );
}



global.SetCarouselIndex=function(index){
    global.CarouselIndex=index;
    var firstElementIndex,secondElementIndex,thirdElementIndex;
    print(global.CarouselIndex)
    var totalPiecesCount=script.mapPiecesTextures.length;
    if(index==0)
    {
        firstElementIndex=totalPiecesCount-1;
        secondElementIndex=index;
        thirdElementIndex=index+1;
    }
    else if(index==totalPiecesCount-1){
       firstElementIndex=index-1;
        secondElementIndex=index;
        thirdElementIndex=0;
    }
    else{
        firstElementIndex=index-1;
        secondElementIndex=index;
        thirdElementIndex=index+1;
    }
   // print('test'+global.piecesCollectedIndexes[0])
         SetCarouselElementTexture(0,firstElementIndex);
         SetCarouselElementTexture(1,secondElementIndex);
         SetCarouselElementTexture(2,thirdElementIndex);
  // print(firstElementIndex.toString()+secondElementIndex.toString()+thirdElementIndex.toString())
   // if(global.piecesCollectedIndexes.lastIndexOf(firstElementIndex)==0)
   // {
    //    print("first collected before")
       global.SetCarouselElementShadowMode(0.5,0)
    //}
   // else if(global.piecesCollectedIndexes.lastIndexOf(firstElementIndex)==-1)
   // {
     //   print("first wasnt collected before")
    //    global.SetCarouselElementShadowMode(0.5,0)
   // }
    var count=global.piecesCollectedIndexes.length
    print(count)
    for(i=0;i<count;i++)
    {
        if(index==global.piecesCollectedIndexes[i])
        {
             global.DraggablePieceCanBeDragged(false)
             global.SetCarouselElementShadowMode(0.2,1)
            break;
        }
        else{
            print("wasnt collected before")
              global.SetCarouselElementShadowMode(1,1)
            global.DraggablePieceCanBeDragged(true)
        }
    }
    /*
     if(global.piecesCollectedIndexes.lastIndexOf(secondElementIndex)==0)
    {
         print("second collected before")
        global.DraggablePieceCanBeDragged(false)
       global.SetCarouselElementShadowMode(0.2,1)
    }
    else(global.piecesCollectedIndexes.lastIndexOf(secondElementIndex)==-1)
    {
          print("second wasnt collected before")
        global.SetCarouselElementShadowMode(1,1)
        global.DraggablePieceCanBeDragged(true)
    }
    */
   // if(global.piecesCollectedIndexes.lastIndexOf(thirdElementIndex)==0)
     //  global.SetCarouselElementShadowMode(0.2,2)    
    //else if(global.piecesCollectedIndexes.lastIndexOf(thirdElementIndex)==-1)
     global.SetCarouselElementShadowMode(0.5,2)
      
}

script.api.CarouselRArrow = function () {
  if (global.gameStarted&&!global.gameFinished) {
        global.tweenManager.startTween(script.carouselRArrow,"RArrowTween",AfterRCarouselTween);
        global.tweenManager.startTween(script.carouselRArrow,"CarouselDissolve");
  }
};

script.api.CarouselLArrow = function () {
  if (global.gameStarted && !global.gameFinished){
        global.tweenManager.startTween(script.carouselLArrow,"LArrowTween",AfterLCarouselTween); 
        global.tweenManager.startTween(script.carouselRArrow,"CarouselDissolve");
    }
};

function AfterRCarouselTween(){
    global.tweenManager.resetObject(script.carouselRArrow,"RArrowTween")
    global.tweenManager.resetObject(script.carouselRArrow,"CarouselDissolve")
     if (global.CarouselIndex < script.mapPiecesTextures.length - 1)
      global.SetCarouselIndex(global.CarouselIndex + 1);
    else global.SetCarouselIndex(0);
}

function AfterLCarouselTween(){
    global.tweenManager.resetObject(script.carouselLArrow,"LArrowTween")
      global.tweenManager.resetObject(script.carouselRArrow,"CarouselDissolve")
     if (global.CarouselIndex >0)
      global.SetCarouselIndex(global.CarouselIndex - 1);
      else
      global.SetCarouselIndex(script.mapPiecesTextures.length - 1)  
}