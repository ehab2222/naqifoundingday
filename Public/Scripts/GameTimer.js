// -----JS CODE-----
//@input float totalTime;
//@input Component.Text timerTxt;

var timerStarted=false;
var timerFinished=false;
var timerCanTick=true;
var currentTime;
var timerFinishedEventFired=false;

script.api.ResetTimer=function(){
    timerStarted=false;
    timerFinished=false;
    currentTime=script.totalTime;
    timerFinishedEventFired=false;
    timerCanTick=true;
    script.timerTxt.text="00 : "+currentTime.toString();
    
}
script.api.StartTimer=function(){
    timerStarted=true;
    currentTime=script.totalTime;
}

var timerUpdate=script.createEvent("UpdateEvent");
var secondDelay=script.createEvent("DelayedCallbackEvent");
timerUpdate.bind(TimerLoop);
secondDelay.bind(TimerLogic);

function TimerLoop(){
   if( global.IsInBackCam&&global.gameStarted&&!global.gameFinished)
    {
    if(timerStarted&&timerCanTick&&!timerFinished){
     timerCanTick=false;
     secondDelay.reset(1);
    }
    if(timerFinished&&!timerFinishedEventFired)
    {
        timerFinishedEventFired=true;
        global.EndMapPuzzleGame();
    }
    }
}

function TimerLogic(){
    if(global.IsInBackCam&&global.gameStarted&&!global.gameFinished)
    {
     currentTime--;
    if(currentTime<10)
     script.timerTxt.text="00 : "+"0"+currentTime.toString();
    else
    script.timerTxt.text="00 : "+currentTime.toString();
    timerCanTick=true;
    if(currentTime<=0)
    timerFinished=true;
    }
}