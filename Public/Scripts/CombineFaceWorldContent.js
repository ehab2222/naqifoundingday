// CombineFaceWorldContent.js
// Version: 0.0.1
// Event: Lens Initialized
// Description: Hides the face content on rear camera and world content on front camera
// @input SceneObject[] faceContent
// @input SceneObject[] worldContent
//@input SceneObject timerUI;
//@input SceneObject maleFemaleUI;


var resetDelayEvnt=script.createEvent("DelayedCallbackEvent");

function onFrontCamEvent(eventData) {
  global.IsInBackCam = false;
script.timerUI.enabled=false;
  for (var i = 0; i < script.faceContent.length; i++) {
    var faceObject = script.faceContent[i];
    if (faceObject) {
      faceObject.enabled = true;
    }
  }

  for (var i = 0; i < script.worldContent.length; i++) {
    var worldObject = script.worldContent[i];
    if (worldObject) {
      worldObject.enabled = false;
    }
  }
  // Stop the back camera game here
}
var cameraFrontEvent = script.createEvent("CameraFrontEvent");
cameraFrontEvent.bind(onFrontCamEvent);

function onBackCamEvent(eventData) {
   script.maleFemaleUI.enabled=false;
    script.timerUI.enabled=true;
  global.IsInBackCam = true;
  for (var i = 0; i < script.faceContent.length; i++) {
    var faceObject = script.faceContent[i];
    if (faceObject) {
      faceObject.enabled = false;
    }
  }

  for (var i = 0; i < script.worldContent.length; i++) {
    var worldObject = script.worldContent[i];
    if (worldObject) {
      worldObject.enabled = true;
    }
  }
    resetDelayEvnt.reset(0.3)
}
resetDelayEvnt.bind(function(eventData){
    global.ResetPuzzleGame();

});

var cameraBackEvent = script.createEvent("CameraBackEvent");
cameraBackEvent.bind(onBackCamEvent);
