// -----JS CODE-----
//@input SceneObject overlayImg;
//@input SceneObject puzzleGameInstructions;
//@input Component.ScriptComponent GameTimerRef;
//@input Component draggablePieceManiuplationComponent;
//@input Component.ScreenTransform[] actualPiecesOnMap;
//@input Component.ScreenTransform draggablePieceTransform;
//@input Asset.Material draggablePieceMat;
//@input Asset.Texture[] mapPiecesTextures;
//@input SceneObject tapPuzzleGameInstruction;
//@input vec3[] draggedPiecesRescaleValues;

//@input SceneObject timerUI;
//@input SceneObject founderDayPromo;
//@input SceneObject puzzleMap;
//@input SceneObject tryAgainTxt;

//@input Component.Image[] carouselElements
//@input SceneObject carouselUI;


var initialDraggablePiecePos = script.draggablePieceTransform.position;


global.EndMapPuzzleGame = function () {
    print("Game Ended")
    global.gameFinished=true;
    print(global.piecesCollectedIndexes.length)
    script.overlayImg.enabled=true;

    // Won Game    
    if(global.piecesCollectedIndexes.length==script.actualPiecesOnMap.length)
    {
        script.timerUI.enabled=false;
        script.founderDayPromo.enabled=true;
        script.puzzleMap.enabled=false;
        script.carouselUI.enabled=false;
    }
    // Game Lost
    else{
        script.overlayImg.enabled=true;
        script.tryAgainTxt.enabled=true;
    }
};

global.DraggablePieceCanBeDragged = function (draggable) {
  if (draggable) script.draggablePieceManiuplationComponent.enabled = true;
  else script.draggablePieceManiuplationComponent.enabled = false;
};

global.IsPieceDraggableNow = function () {
  print(script.draggablePieceManiuplationComponent.enabled);
  return script.draggablePieceManiuplationComponent.enabled;
};


function HideAllMapPieces(){
for(i=0;i<script.actualPiecesOnMap.length;i++)
    script.actualPiecesOnMap[i].getSceneObject()
    .getComponent("Component.Image").enabled=false;
}

function SetDraggablePieceVisibility(visible) {
  var currColor = script.draggablePieceTransform
    .getSceneObject()
    .getComponent("Component.Image").mainPass.baseColor;
  var alpha;

  if (visible) {
    alpha = 1;
  } else {
    alpha = 0;
  }
  script.draggablePieceTransform
    .getSceneObject()
    .getComponent("Component.Image").mainPass.baseColor = new vec4(
    currColor.r,
    currColor.g,
    currColor.b,
    alpha
  );
}



//Reset Game
global.ResetPuzzleGame = function () {
  script.timerUI.enabled=true; 
  global.piecesCollectedIndexes = [];
  HideAllMapPieces();
  global.ResetCarousel();
  script.tryAgainTxt.enabled=false;
  global.gameFinished=false;
  script.founderDayPromo.enabled=false;
  SetDraggablePieceVisibility(false);
  global.DraggablePieceCanBeDragged(false);
  // add a little delay for the carousel start animation
  print("Puzzle Game Reset");
  global.gameStarted = false;
  global.FirstPuzzlePieceTapTrigger = false;
  script.overlayImg.enabled = true;
  script.tapPuzzleGameInstruction.enabled = true;
  script.puzzleGameInstructions.enabled = true;
  script.puzzleMap.enabled=true;
  script.carouselUI.enabled=true;
  script.GameTimerRef.api.ResetTimer();
};

script.api.StartPuzzleGame = function () {
  if (!global.gameStarted) {
    global.DraggablePieceCanBeDragged(true);
    script.overlayImg.enabled = false;
    global.gameStarted = true;
    script.GameTimerRef.api.StartTimer();
    script.tapPuzzleGameInstruction.enabled = false;
    script.puzzleGameInstructions.enabled = false;
  }
};

script.api.DragMapPiece = function () {
  if (global.gameStarted&&global.IsPieceDraggableNow()&&!global.gameFinished) {
   // if(!global.FirstPuzzlePieceTapTrigger)
   // {
     //  global.FirstPuzzlePieceTapTrigger = true;
    //   script.GameTimerRef.api.StartTimer();
   // }
   // script.tapPuzzleGameInstruction.enabled = false;
   // script.puzzleGameInstructions.enabled = false;
    // Shows Draggable Piece
    SetDraggablePieceVisibility(true);
    //Shadow mode set 0.5
     global.SetCarouselElementShadowMode(0.5,1);
    script.draggablePieceTransform.scale =
      script.draggedPiecesRescaleValues[global.CarouselIndex];
  //  print(script.draggablePieceTransform.scale);
    script.draggablePieceMat.mainPass.baseTex =
      script.mapPiecesTextures[global.CarouselIndex];
  }
};




script.api.DropMapPiece = function () {
  if (global.gameStarted&&global.IsPieceDraggableNow()&&!global.gameFinished) {
     global.DraggablePieceCanBeDragged(true)
    var distanceToTarget = script.draggablePieceTransform.position.distance(
      script.actualPiecesOnMap[global.CarouselIndex].position
    );
    script.draggablePieceTransform.position = initialDraggablePiecePos;
    script.draggablePieceTransform.scale = new vec3(1, 1, 1);
    SetDraggablePieceVisibility(false);
    if (distanceToTarget < 1.5) {
      print("Collected");
      global.piecesCollectedIndexes.push(global.CarouselIndex);
      script.actualPiecesOnMap[
        global.CarouselIndex
      ].getSceneObject()
       .getComponent("Component.Image").enabled=true;
       // Check if all pieces collected 
        if(global.piecesCollectedIndexes.length==script.actualPiecesOnMap.length)
         {
             global.EndMapPuzzleGame();       
         }
      // Go to next element or to the first element if this is the last one      
      if(global.CarouselIndex<script.actualPiecesOnMap.length-1)
      var x = global.CarouselIndex + 1;
      else
      var x=0;
      global.SetCarouselIndex(x);
    } else {
      // IF NOT PLACED IN MAP PLACE
       global.SetCarouselElementShadowMode(1,1);
    }
  }
};
