// -----JS CODE-----

//@input SceneObject maleFemaleIconsContainer;
//@input SceneObject maleIcon;
//@input SceneObject femaleIcon;

//@input SceneObject[] maleAccessories;
//@input SceneObject[] femaleAccessories;
//@input Component.Image flipCamTxt;
//@input SceneObject personOcculuder;
//@input SceneObject maleFemaleUI;

var flipCamTxtHideDelay=script.createEvent("DelayedCallbackEvent");
flipCamTxtHideDelay.bind(HideFlipCamDelayCallback);

var flipCamTxtShowDelay=script.createEvent("DelayedCallbackEvent");
flipCamTxtShowDelay.bind(ShowFlipCamDelayCallback);

function HideFlipCamDelayCallback(){
    script.flipCamTxt.enabled=false;
}

function ShowFlipCamDelayCallback(){
    script.flipCamTxt.enabled=true;
     flipCamTxtHideDelay.reset(3);
}
script.api.ResetFrontCamExperience=function(){
    SetMaleAccessoriesVisibility(false);
    SetFemaleAccessoriesVisibility(false);
    script.personOcculuder.enabled=false;
    script.flipCamTxt.enabled=false;
    script.maleFemaleIconsContainer.enabled=true
    global.tweenManager.startTween(script.maleIcon,"ShowMaleIconTween",CheckBackCam) 
    global.tweenManager.startTween(script.femaleIcon,"ShowFemaleIconTween",CheckBackCam) 
    print("reset front cam experience")
}

script.api.SelectMale=function(){
    script.personOcculuder.enabled=true;
   global.tweenManager.startTween(script.maleIcon,"HideMaleIconTween",ShowFlipCameraTxt)
   global.tweenManager.startTween(script.femaleIcon,"HideFemaleIconTween")
   SetMaleAccessoriesVisibility(true);
   SetFemaleAccessoriesVisibility(false);
}

script.api.SelectFemale=function(){
   script.personOcculuder.enabled=true;
   global.tweenManager.startTween(script.maleIcon,"HideMaleIconTween")
   global.tweenManager.startTween(script.femaleIcon,"HideFemaleIconTween",ShowFlipCameraTxt)
   SetMaleAccessoriesVisibility(false);
   SetFemaleAccessoriesVisibility(true);
}

function SetMaleAccessoriesVisibility(visible)
{
     for(var i=0;i<script.maleAccessories.length;i++ )
    {
     script.maleAccessories[i].enabled=visible;
   }
}

function SetFemaleAccessoriesVisibility(visible)
{
     for(var i=0;i<script.femaleAccessories.length;i++ )
    {
      script.femaleAccessories[i].enabled=visible;
   }
}

function ShowFlipCameraTxt(){
    flipCamTxtShowDelay.reset(1);
    //script.personOcculuder.enabled=true;
}

function CheckBackCam(){
    if( global.IsInBackCam==true)
    {
        script.maleFemaleUI.enabled=false;
    }
}